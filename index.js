'use strict';

var mongoose = require('mongoose');
mongoose.connect('mongodb://jshealth:jshealth@ds045521.mongolab.com:45521/reporting');
var natural = require('natural')
var tokenizer = new natural.WordTokenizer();

var Food = require('./model')(mongoose);
var nutrixService = require('./nutrix');

console.log('up and running.')
Food.find({}, function(err, foods) {

  if(err) return console.log(err);
  console.log('found', foods);

  foods.forEach(function(food) {
    var tokenized = tokenizer.tokenize(food.name);
    console.log(tokenized);
    nutrixService.findFood(tokenized, function(err, results) {
      if(err) return console.log(err);
      console.log(results.originalUrl, '==============================================');
      food.originalUrl = results.originalUrl;

      results.data.hits.forEach(function(item){
        console.log(food.name);
        console.log(item.fields.item_name)
        item.distanceScore = natural.LevenshteinDistance(item.fields.item_name, food.name);
        food.resultSet.push(item);
      })

      food.save(function(err, doc) {
        if(err) return console.log(err);
        return console.log(doc);
      });



    })


  });



})
