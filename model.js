var mongoose = require('mongoose');

module.exports = function(mongoose) {

  foodSchema = new mongoose.Schema({
    name: String,
    originalUrl: String,
    hasResult: Boolean,
    resultSet:[{}]
  });

  return mongoose.model('foodItems', foodSchema);
}



