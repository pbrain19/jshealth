'use strict';

var req = require('request');

class Nutrix {

  constructor() {
    this.appId = '16fbb525';
    this.appKey = 'd9f632c55f265dc5436aa7c9e6277285';
    this.baseUrl = 'https://api.nutritionix.com/v1_1/search/';
    this.credentials = ['appId=' + this.appId, 'appKey=' + this.appKey ].join('&');
    console.log('credentials[=-====', this.credentials);
  }

  findFood(foodName, cb) {
    var requestURl = this.baseUrl + foodName + '?' + this.credentials;
    req(requestURl, function (error, response, body) {
      var payload = {
        data: JSON.parse(body),
        originalUrl: requestURl
      }
      cb(error, payload);
    });
  }

}

var nutrix = new Nutrix();
module.exports = nutrix;